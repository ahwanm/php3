<?php
//start the session
session_start();
//include database connection file
include_once("php/connect.php");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <style media="screen">
    @media (min-width: 1000px){
      html,body {width: 100%; height: 100%; }
      .full-height {  height: 100%; }}
    @media (max-width: 1000px){
      .bgg{
        background-color: #007bff;
        padding-bottom: 4rem;
      }
    }
    </style>
    <meta charset="utf-8">
    <title>Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- External CSS -->
    <link rel="stylesheet" href="css/master.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />
  </head>
  <body class="">
    <?php
      // define variables and set to empty values
      $emailErr = "";
      $passwordErr = "";
      $email = "";
      $password = "";
      $error = false;
      $password = test_input($_POST["password"]);
      if (isset($_POST["submit"])) {
          if (empty($_POST["email"])) {
              $emailErr = "Required";
              $error = true;
          } else {
              $email = test_input($_POST["email"]);
              // check if e-mail address is well-formed
              if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $emailErr = "Invalid email format";
                  $error = true;
              }
              $query = "SELECT count(*) as nos FROM `details` where email = ";
              $query .= "'$email'";
              $result = mysqli_query($conn, $query);
              $row = mysqli_fetch_assoc($result);
              if ($row['nos'] < 1) {
                  $emailErr = "No such email already exists";
                  $error = true;
              } else {
                  $query = "SELECT password FROM `details` where email = ";
                  $query .= "'$email'";
                  $result = mysqli_query($conn, $query);
                  $row = mysqli_fetch_assoc($result);
                  $hash = $row["password"];
                  $verify = password_verify($password, $hash);
                  if (!($verify)) {
                      $passwordErr = "Invalid Password";
                      $error = true;
                  } else {
                      $_SESSION['email'] = $email;
                      header("location:php/resume.php");
                  }
              }
          }
      }
      function test_input($data)
      {
          $data = trim($data);
          $data = stripslashes($data);
          $data = htmlspecialchars($data);
          return $data;
      }
      ?>
    <!-- Header -->
    <div class="full-height header full-height">
      <div class="full-height bg-light">
        <div class="row ">
          <div class="sidebar col col-lg-6">
            <div class="bgg pt-5 pl-5 pr-5 row">
              <div id='aaa' class="col-lg-8 text-light ">
                <h1>Resume</h1>
                <p>Help us help you build your own resume website using HTML, CSS, BootStrap, JavaScript and PHP.</p>
                <button type="button" class="mt-3 btn btn-lg btn-light" onclick="location.href = 'php/signup.php';">Sign Up</button>
              </div>
            </div>
            <div class="row">
                <div class="col text-right">
                  <img src="https://pngimg.com/uploads/robot/robot_PNG57.png"  style="  width: 100%; " class="mt-5" >
                </div>
            </div>
          </div>
        </div>
        <div class="full-height container-fluid">
          <div class="full-height row">
            <div class="full-height bg-primary col col-lg-4 sig col-12"></div>
            <div class="full-height sig col col-lg-2 col-12 "></div>
            <div class="full-height sig col col-lg-4 col-12">
              <div class="signin">
                <h2 class="">Signin to Profile</h2>
                <p class="mb-5">Don't have an account? <a href="php/signup.php">Register</a></p>
                <!-- Signup Form -->
                <form method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" value="<?php echo($emailErr=="") && isset($_POST["email"]) ? $_POST["email"] : ''; ?>" placeholder="abc@xyz.com" name="email" >
                    <div class="error"> <?php echo $emailErr;?></div>
                  </div>
                  <div class="form-group mb-4">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" value="<?php echo($passwordErr=="") && isset($_POST["password"]) ? $_POST["password"] : ''; ?>" placeholder="Enter your password" name="password" >
                    <div class="error"> <?php echo $passwordErr;?></div>
                  </div>
                  <hr class="mb-3">
                  <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit" value="Process">Log In</button>
                </form>
              </div>
            </div>
            <div class="full-height sig col col-lg-2 col-12"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Font Awesome  -->
    <script src="https://kit.fontawesome.com/31875a1568.js" crossorigin="anonymous"></script>
    <!-- External JavaScript -->
    <script src="js/signup.js"></script>
    <!-- BootStrap Script -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  </body>
</html>
<?php
//include database dis-connection file
include_once("php/disconnect.php");
?>
